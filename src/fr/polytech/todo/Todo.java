package fr.polytech.todo;

public class Todo {
	
	private long id;
	private String name;
	private String description;
	public static long currentId;
	
	@Override
	public String toString(){
		return "["+this.id+"] "+this.name+" : "+this.description;
	}
	
	//Constructor
	
	public Todo(String name, String description) {
		
		if (currentId == 0) {
			currentId = 1;
		}
		this.id = currentId;
		currentId = currentId+1;
		//TODO : pour l'instant cette méthode ne sauvegarde pas la liste
		
		this.name = name;
		this.description = description;
	}
	
	
	public Todo(){}
	
	
	//MAIN
	public static void main(String[] args){
		Todo t1 = new Todo("Courses","Aller faire les courses");
		Todo t2 = new Todo("Courses","Aller faire les courses");
		String string = t1.toString();
		String string2 = t2.toString();
		System.out.println(string + string2);
	}
	
	
	
	public long getId() {
		return this.id;
	}
	
	public String getName() {
		return this.name;
	}
	
	public String getDescription() {
		return this.description;
	}
	
	public void setId(long id) {
		this.id = id;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public void setDescription(String description) {
		this.description = description;
	}
	
	
}
