package fr.polytech.todo.servlet;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fr.polytech.todo.Todo;

/**
 * Servlet implementation class TodoServlet
 */
@WebServlet("/Todo")
public class TodoServlet extends HttpServlet {
	
	private List<Todo> todos = new ArrayList<Todo>();
	
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public TodoServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html");
		RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/TodoForm.jsp");
		dispatcher.forward(request, response);	
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		if(request.getParameter("action").equals("saveTask")){
			
			String taskName = request.getParameter("taskName");
			String taskDescription = request.getParameter("taskDescription");
		
			Todo todo = new Todo(taskName,taskDescription);
			this.todos.add(todo);
			
			request.setAttribute("taskList",this.todos);

			RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/TodoForm.jsp");
			dispatcher.forward(request, response);
		}
	}


}
